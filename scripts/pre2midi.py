#!/usr/bin/env python3
from sys import argv, stdin
from mido import (
        MidiFile, MidiTrack, Message, MetaMessage, bpm2tempo, second2tick
    )

_, outfile_name, ppq, *program = argv
mid = MidiFile(ticks_per_beat=int(ppq))
track = MidiTrack()
mid.tracks.append(track)

tempo = -1

def ticked(seconds):
    return round(second2tick(seconds, mid.ticks_per_beat, bpm2tempo(tempo)))

def linearize_dB(dB):
    dB /= 100
    return round(127 * (10 ** (-5 * (1-dB))))

def appender(msg):
    print(msg)
    track.append(msg)

for i, prog in enumerate(program):
    appender(Message('program_change', channel=i, program=int(prog), time=0))

last_offset = 0
active_notes = set()
channels = {}

for line in stdin:
    line_seg = line.split('\t')
    args = [None] * 5
    for i, part in enumerate(line_seg):
        args[i] = part

    offset, channel, pitch, velocity, comment = args

    try:
        offset = float(offset)
    except ValueError:
        continue

    offset -= last_offset
    
    if channel.startswith("# CLOCK(bpm="):
        tempo=float(channel[12:-2])
        appender(MetaMessage(
            'set_tempo', tempo=bpm2tempo(tempo), time=ticked(offset)
        ))

    try:
        pitch = int(pitch)+20
    except TypeError:
        continue

    channel = channels.setdefault(channel, len(channels))

    try:
        velocity_dB = velocity != str(int(velocity))
    except ValueError:
        velocity_dB = True

    if velocity_dB:
        velocity = linearize_dB(float(velocity))
    else:
        velocity = int(velocity)

    if comment.startswith('# RELEASE'):
        msg_type = 'note_off'
        if pitch in active_notes:
            active_notes.remove(pitch)
    else:
        msg_type = 'note_on'
        if pitch in active_notes:
            appender(Message(
                'note_off', channel=0, note=pitch, velocity=velocity,
                time=ticked(offset)
            ))
            active_notes.remove(pitch)
            offset = 0
        active_notes.add(pitch)

    appender(Message(
        msg_type, channel=channel, note=pitch, velocity=velocity, time=ticked(offset)
    ))

    last_offset += offset

mid.save(outfile_name)
