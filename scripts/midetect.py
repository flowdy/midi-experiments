from rtmidi import RtMidiIn
from time import sleep

NOTEMAP = [
        'A0', 'A#/Bb0', 'B0',
        'C1', 'C#/Db1', 'D1', 'D#/Eb1', 'E1', 'F1', 'F#/Gb1', 'G1', 'G#/Ab1', 'A1', 'A#/Bb1', 'B1',
        'C2', 'C#/Db2', 'D2', 'D#/Eb2', 'E2', 'F2', 'F#/Gb2', 'G2', 'G#/Ab2', 'A2', 'A#/Bb2', 'B2',
        'C3', 'C#/Db3', 'D3', 'D#/Eb3', 'E3', 'F3', 'F#/Gb3', 'G3', 'G#/Ab3', 'A3', 'A#/Bb3', 'B3',
        'C4', 'C#/Db4', 'D4', 'D#/Eb4', 'E4', 'F4', 'F#/Gb4', 'G4', 'G#/Ab4', 'A4', 'A#/Bb4', 'B4',
        'C5', 'C#/Db5', 'D5', 'D#/Eb5', 'E5', 'F5', 'F#/Gb5', 'G5', 'G#/Ab5', 'A5', 'A#/Bb5', 'B5',
        'C6', 'C#/Db6', 'D6', 'D#/Eb6', 'E6', 'F6', 'F#/Gb6', 'G6', 'G#/Ab6', 'A6', 'A#/Bb6', 'B6',
        'C7', 'C#/Db7', 'D7', 'D#/Eb7', 'E7', 'F7', 'F#/Gb7', 'G7', 'G#/Ab7', 'A7', 'A#/Bb7', 'B7',
        'C8'
    ]

midin = RtMidiIn()
ports = midin.getPortCount()
if ports == 0:
    print("No MIDI ports available.")
    exit(1)
else:
    for i in range(ports):
        print("{:02d} {}".format(i, midin.getPortName(i)))
    selected_port = None
    while selected_port is None:
        port = input("Select Midi port [0-{:d}]: ".format(ports-1))
        if port == '':
            exit(1)
        try:
            selected_port = int(port)
            if 0 <= selected_port < ports:
                break
            else:
                selected_port = None
                raise ValueError("Off range")
        except (TypeError, ValueError):
            print("Unexpected input: " + str(e))

midin.openPort(selected_port)

slept = 0
passed_notes = []
pressed_notes = {}
timer = 0
while slept < 20 or pressed_notes:
    msg = midin.getMessage()
    if msg is None:
        slept += 1
        sleep(1)
        continue

    number = msg.getNoteNumber()
    if number:
        note = NOTEMAP[number - 21]

    if msg.isNoteOn():
        pressed_notes[note] = (msg.getChannel(), timer + msg.getTimeStamp(), msg.getVelocity())
        print("Pressed key {} after ca. {:d} seconds".format(note, slept))
    elif msg.isNoteOff():
        try:
            channel, pressed, velocity = pressed_notes.pop(note)
        except KeyError as e:
            print(f"CANNOT FIND {note} in pressed_notes, released already?")
            continue
        passed_notes.append( (channel, pressed, note, velocity, timer + msg.getTimeStamp() - pressed) )
        print("Released key {} after ca. {:d} seconds".format(note, slept))
    else:
        print("Ignore " + str(msg))

    slept = 0

    timer += msg.getTimeStamp()

for note in sorted(passed_notes, key=lambda x: x[1]):
    print("On channel {} at {:.3f}s note {} pressed with velocity {:d} {:.3f}s long".format(*note))

midin.closePort()
